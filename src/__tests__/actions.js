import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import * as actions from '../actions';
import * as types from '../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
    it('Should create a user to fetch a repo with', () => {
        const user = 'aderaaij';
        const expectedAction = {
            type: types.SELECT_USER,
            user,
        };
        expect(actions.selectUser(user)).toEqual(expectedAction);
    });

    it('Should swith a boolean to let us know fetching started', () => {
        const expectedAction = {
            type: types.REQUEST_USERDATA,
        };
        expect(actions.requestUserData()).toEqual(expectedAction);
    });
});