import {
    SELECT_USER,
    REQUEST_USERDATA,
    RECEIVE_USERDATA,
    RECEIVE_USERDATA_ERROR
} from './constants/ActionTypes';

export function selectUser(user) {
    return {
        type: SELECT_USER,
        user,
    };
}

export function requestUserData() {
    return {
        type: REQUEST_USERDATA,
    };
}

function receiveUserData(json) {
    return {
        type: RECEIVE_USERDATA,
        userData: json,
    };
}

function receiveUserDataErr(error) {
    return {
        type: RECEIVE_USERDATA_ERROR,
        error,
    };
}

export function fetchChange(dollarAmount) {
    return dispatch => {
        dispatch(requestUserData());
        return fetch(`https://changemaker-api.tiagomatic.com/?dollarAmount=${dollarAmount}`)
            .then(res => res.json())
            .then(json => dispatch(receiveUserData(json)))
            .catch(err => dispatch(receiveUserDataErr(err)));
    };
}


export function fetchUserAndRepos(user) {
    return (dispatch, getState) => {
        return dispatch(fetchChange(user)).then(() => {
            const { currentUserData } = getState();
            if (
                !currentUserData.isFetching &&
                currentUserData.userData.message
            ) {
                return;
            }
        });
    };
}
