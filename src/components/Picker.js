import React from 'react';
import PropTypes from 'prop-types';

const Picker = props => {
    let input;
    const { onSubmit } = props;
    return (
        <span>
            <h1>Changemaker 5000</h1>
            <p>Type in dollar amount below to get change back</p>
            <form class="mx-auto" style={{width: '330px'}}
                onSubmit={e => {
                    e.preventDefault();
                    if (input.value !== '') {
                        onSubmit(input.value);
                    }
                }}
            >
                $<input
                    type="text"
                    ref={node => {
                        input = node;
                    }}
                />
                <input type="submit" value="Chaching" />
            </form>
        </span>
    );
};

Picker.propTypes = {
    onSubmit: PropTypes.func.isRequired,
};

export default Picker;
