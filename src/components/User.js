import React from 'react';
import PropTypes from 'prop-types';

const User = props => {
    const { currentUserData } = props;
    const { userData } = currentUserData;
    console.log(currentUserData);
    return (
        <div>
            <h2 class="mt-3 mb-3">Your change is:</h2>
            <p> {userData["silver-dollar"]} Silver Dollars</p>
            <p> {userData["half-dollar"]} Half Dollars</p>
            <p> {userData.quarter} Quarters</p>
            <p> {userData.dime} Dimes</p>
            <p> {userData.nickel} Nickels</p>
            <p> {userData.penny} Pennies</p>


        </div>
    );
};

User.propTypes = {
    currentUserData: PropTypes.object.isRequired,
    userRepos: PropTypes.object.isRequired,
};

export default User;
